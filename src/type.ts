export type ComboEntity = {
  consumption?: string;
  production?: string;
};

export type EntityType = "battery" | "gas" | "grid" | "solar" | "water";

export type MultipleSolar = {
  entity: string;
  label?: string;
  color?: string;
}
